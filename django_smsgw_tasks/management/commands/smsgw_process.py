from django.core.management.base import BaseCommand

from django_smsgw_tasks.django_smsgw_tasks.tasks import smsgw_process


class Command(BaseCommand):
    help = "Run task smsgw_process"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        smsgw_process()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
