from celery import shared_task

from django_smsgw_models.django_smsgw_models.models import *
from django_smsgw_yeastar_tg.django_smsgw_yeastar_tg.func import (
    send_sms as yeastar_tg_send_sms,
)


@shared_task(name="smsgw_process")
def smsgw_process():
    unsent_messages = SmsGwMessage.objects.filter(sent=False)

    for unsent_message in unsent_messages:
        if yeastar_tg_send_sms(
            unsent_message.destination_nr.as_e164, unsent_message.text
        ):
            unsent_message.sent = True
            unsent_message.sent_date = datetime.now()
            unsent_message.save()

    return
